# onesec
onesec is a general purpose React.js animation library.

The goal of the library is to provide working out-of-the-box animation components for list/reflow, enter, and exit transitions. The idea was inspired by Vue.js `Transition` and `TransitionGroup` components, which have better transition animation support than React's components.

Vue docs are [here](https://vuejs.org/v2/guide/transitions.html).

React docs are [here](https://reactjs.org/docs/animation.html#high-level-api-reactcsstransitiongroup).

**This project is on hold while I pursue other projects during the academic semester.**

### Completed
- list/reflow transitions based on FLIP technique

### TODO
- enter/exit support using React's transition components
