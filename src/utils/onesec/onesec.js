import React from 'react';
import ReactDOM from 'react-dom';

const attachOnesec = (Component) => {
  if (!isStateful(Component)) {
    throw new Error('Onesec can only be attached to stateful components');
  }
  else {
    return wrapComponent(Component);
  }
};

const isStateful = (Component) => {
  return typeof Component === 'string' || Component.prototype.render;
}

const wrapComponent = (Component) => {
  const OnesecComponent = class extends React.Component {
    constructor(props) {
      super(props);
      this.ref = React.createRef();
      this.prevRect = undefined;
      this.componentWillNeedPaint = this.componentWillNeedPaint.bind(this);
      this.componentWillPaint = this.componentWillPaint.bind(this);
    }

    componentDidMount() {
      this.componentWillNeedPaint();
    }

    componentWillNeedPaint() {
      const node = this.getCurrentNode();
      if (node) {
        this.first(node.getBoundingClientRect());
      }
    }

    componentWillPaint() {
      const node = this.getCurrentNode();
      if (node) {
        this.play(node, false);
        requestAnimationFrame(() => {
          const nextRect = this.last(node);
          this.invert(node, this.prevRect, nextRect);
          requestAnimationFrame(() => {
            this.play(node);
          });
          this.first(nextRect);
        });
      }
    }

    first(nextRect) {
      this.prevRect = nextRect;
    }

    last(node) {
      return node.getBoundingClientRect();
    }

    invert(node, prevRect, nextRect) {
      const topDiff = prevRect.top - nextRect.top;
      const leftDiff = prevRect.left - nextRect.left;
      node.style.transition = 'none';
      node.style.transform = `translate(${leftDiff}px, ${topDiff}px)`;
    }

    play(node, transition=true) {
      node.style.transition = transition ? '' : 'none';
      node.style.transform = 'translate(0px, 0px)';
    }

    getCurrentNode() {
      if (this.ref.current) {
        return ReactDOM.findDOMNode(this.ref.current);
      }
    }

    render() {
      return (
        <Component
          ref={this.ref}
          onesec={this.componentWillPaint}
          willOnesec={this.componentWillNeedPaint}
          {...this.props}
        />
      );
    }
  }

  Object.defineProperty(OnesecComponent, 'name', {
    value: 'Onesec' + Component.name
  });
  return OnesecComponent;
}

export default attachOnesec;
