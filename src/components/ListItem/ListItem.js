import React, { Component } from 'react';
import attachOnesec from '../../utils/onesec';

import './ListItem.css';

class ListItem extends Component {
  getSnapshotBeforeUpdate() {
    const { willOnesec } = this.props;
    willOnesec();
    return null;
  }

  componentDidUpdate() {
    const { onesec } = this.props;
    onesec();
  }

  render() {
    return (
      <div
        className="list-item"
        onClick={this.props.onClick}
      >{ this.props.item }</div>
    );
  }
}

export default attachOnesec(ListItem);
