import React, { Component } from 'react';
// import Block from '../Block';
import ListItem from '../ListItem';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.list = [
      1, 2, 3, 4, 5,
      6, 7, 8, 9, 10,
      11, 12, 13, 14, 15,
      16, 17, 18, 19, 20
    ];
  }

  render() {
    const listContent = this.list.map((item, index) => {
      return (
        <ListItem
          key={item}
          item={item}
          onClick={() => {
            this.removeItem(item);
          }}
        />
      );
    });

    return (
      <div style={{maxWidth: '160px'}}>
        { listContent }
      </div>
    );
  }

  removeItem(item) {
    const idx = this.list.indexOf(item);
    if (idx > -1) {
      this.list.splice(idx, 1);
      this.forceUpdate();
    }
  }
}

export default App;
