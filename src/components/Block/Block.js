import React, { Component } from 'react';
import attachOnesec from '../../utils/onesec';
import './Block.css';

class Block extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shifted: false
    };
  }

  getSnapshotBeforeUpdate() {
    const { willOnesec } = this.props;
    willOnesec();
    return null;
  }

  componentDidUpdate() {
    const { onesec } = this.props;
    onesec();
  }

  render() {
    return (
      <div
        className={'block' + (this.state.shifted ? ' shifted' : '')}
        onClick={() => {
          this.setState({ shifted: !this.state.shifted });
        }}
      ></div>
    );
  }
}

export default attachOnesec(Block);
